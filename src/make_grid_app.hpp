#pragma once

#include "vtkImGuiSDL2OpenGLRenderWindow.h"
#include "vtkImGuiSDL2RenderWindowInteractor.h"

#ifdef __EMSCRIPTEN__
// For emscripten, instead of using glad we use its built-in support for OpenGL:
#include <GL/gl.h>
#include <emscripten_browser_file.h>
#include <emscripten/bind.h>
#endif

#include <vtkActor.h>
#include <vtkInteractorStyleSwitch.h>
#include <vtkNew.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkCamera.h>

#include <vtkDataSetMapper.h>
#include <vtkLine.h>
#include <vtkProperty.h>
#include <vtkSmartPointer.h>

#include "bhoomika_unstructured_mesh.hpp"
#include "ImGuiFileDialog.h"

#ifdef SHOW_EXAMPLE
  #include "example_vtk.hpp"
#endif

/**
 * @brief
 *
 */
struct ImGuiRadioTypeLevel
{
  int selected = 5;
};

/**
 * @brief
 *
 */
struct ImGuiRadioTypeColour
{
  int selected = 0;
};

#ifdef __EMSCRIPTEN__
  /**
   * @brief
   *
   * @param filename
   * @param mime_type
   * @param buffer
   * @param callback_data
   */
  void handle_upload_file(
      // the filename of the file the user selected
      std::string const &filename,
      // the MIME type of the file the user selected, for example "image/png"
      std::string const &mime_type,
      // the file's content is exposed in this string_view - access the data with buffer.data() and size with buffer.size().
      std::string_view buffer,
      // optional callback data - identical to whatever you passed to handle_upload_file()
      void *callback_data = nullptr
    ) {
      std::ofstream fout(filename, std::ios_base::out|std::ios_base::binary);
      fout.write(buffer.data(), buffer.size());
      fout.close();
    }
#endif

/**
 * @brief
 *
 */
struct BhooMikaMeshApp {

  vtkNew<vtkRenderer> m_renderer;
  vtkNew<vtkImGuiSDL2OpenGLRenderWindow> m_renderer_window;
  vtkNew<vtkImGuiSDL2RenderWindowInteractor> m_renderer_interactor;
  vtkNew<vtkInteractorStyleSwitch> m_interactor_style;

#ifdef SHOW_EXAMPLE
  ExampleConeSpike m_cone_spike;
  ExampleUnstructuredMesh m_unst_mesh;
#endif
  BhooMikaUnstructuredMesh m_gc_unst_mesh;

  /**
   * @brief Construct a new Solver App object
   *
   */
  BhooMikaMeshApp() {

    // Setup the rendering context and window
    m_renderer_window->AddRenderer(m_renderer);
    m_renderer_window->SetWindowName("BhooMika");
    m_renderer_interactor->SetRenderWindow(m_renderer_window);

    // Setup style
    m_interactor_style->SetCurrentStyleToTrackballCamera();
    m_interactor_style->SetDefaultRenderer(m_renderer);
    m_renderer_interactor->SetInteractorStyle(m_interactor_style);

    // Render the initial setup
    m_renderer->SetBackground(0.2, 0.3, 0.4);
    m_renderer_window->SetSize(800, 800);
    m_renderer_interactor->Initialize();
    auto camera = m_renderer->GetActiveCamera();
    camera->ParallelProjectionOn();

    m_gc_unst_mesh.AddToRenderer(*m_renderer);
    m_renderer->ResetCamera();
  }

  /**
   * @brief Get the Generic Window Id object
   *
   * @return void*
   */
  inline void *GetGenericWindowId() {
    return m_renderer_window->GetGenericWindowId();
  }

  /**
   * @brief Set the Imgui I O object
   *
   * @param io
   */
  inline void SetImguiIO(ImGuiIO &io) {
    m_renderer_interactor->SetImguiIO(&io);
  }

  /**
   * @brief
   *
   */
  inline void Render() { m_renderer_window->Render(); }

  inline bool PushProcessPop(SDL_Event &event) {
    bool done;
    m_renderer_window->PushContext();
    done = m_renderer_interactor->ProcessEvent(&event);
    m_renderer_window->PopContext();
    return done;
  }

  /**
   * @brief
   *
   */
  void RenderViewerOptions(){
    static ImGuiRadioTypeLevel mesh_level;
    static ImGuiRadioTypeColour edge_colour;

    ImGui::Begin("Mesh View");

#ifdef __EMSCRIPTEN__
    if(ImGui::Button("download-mesh")) {
      emscripten::val::global("window").call<void>("offerFileAsDownload", std::string("bhoomika.h5"), std::string("mime/type"));
    }
#endif

#ifdef SHOW_EXAMPLE
    static bool show_example_spike = false, show_example_unmesh = false;
    ImGui::Checkbox("Example spike", &show_example_spike);
    ImGui::SameLine();
    ImGui::Checkbox("Example umesh", &show_example_unmesh);
    if(show_example_spike)
      m_cone_spike.AddToRenderer(*m_renderer);
    else
      m_cone_spike.RemoveFromRenderer(*m_renderer);
    if(show_example_unmesh)
      m_unst_mesh.AddToRenderer(*m_renderer);
    else
      m_unst_mesh.RemoveFromRenderer(*m_renderer);
#endif

    ImGui::Separator();
    ImGui::Text("Select mesh level to view");
    if (ImGui::BeginTable("Mesh level", 3)) {
      for(int irow=0; irow<2; ++irow) {
        ImGui::TableNextRow();
        for(int icol=0; icol<3; ++icol) {
          ImGui::TableNextColumn();
          auto level = irow * 3 + icol;
          ImGui::TableSetColumnIndex(icol);
          if (ImGui::RadioButton(fmt::format("mesh-level-{}", level).c_str(),
              &mesh_level.selected, level)) {
            m_gc_unst_mesh.LoadMeshLevel(mesh_level.selected, edge_colour.selected);
            //m_renderer->ResetCamera();
          }
          if(icol != 2)
            ImGui::SameLine();
        }
      }
      ImGui::EndTable();
    }

    ImGui::Separator();
    ImGui::Text("Select edge colour to view");
    if (ImGui::BeginTable("Edge colour", 3)) {
      for(int irow=0; irow<2; ++irow) {
        ImGui::TableNextRow();
        for(int icol=0; icol<3; ++icol) {
          ImGui::TableNextColumn();
          auto level = irow * 3 + icol;
          ImGui::TableSetColumnIndex(icol);
          if (ImGui::RadioButton(fmt::format("edge-colour-{}", level).c_str(),
              &edge_colour.selected, level)) {
            m_gc_unst_mesh.LoadMeshLevel(mesh_level.selected, edge_colour.selected);
            //m_renderer->ResetCamera();
          }
          if(icol != 2)
            ImGui::SameLine();
        }
      }
      ImGui::EndTable();
    }
    ImGui::End();
  }

};
