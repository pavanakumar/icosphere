#pragma once

#include <vtkUnstructuredGrid.h>
#include <vtkColor.h>
#include <vtkColorSeries.h>
#include <vtkNamedColors.h>
#include <vtkCellType.h>
#include <vtkPoints.h>

#include "edge_mesh.hpp"

static const char *EDGE_COLOUR_MAP[] = {"black", "red", "yellow", "cyan", "blue", "green"};

/**
 * @brief
 *
 */
struct BhooMikaUnstructuredMesh {

  edge_mesh<int, int, double> m_bmesh;
  vtkNew<vtkPoints> points;
  vtkNew<vtkUnstructuredGrid> ugrid;
  vtkNew<vtkDataSetMapper> ugridMapper;
  vtkNew<vtkActor> ugridActor;
  std::vector<vtkNew<vtkUnstructuredGrid>> ugrid_edge;
  std::vector<vtkNew<vtkDataSetMapper>> ugridMapper_edge;
  std::vector<vtkNew<vtkActor>> ugridActor_edge;
  vtkNew<vtkNamedColors> colors;

  /**
   * @brief Construct a new Example Unstructured Mesh object
   *
   */
  BhooMikaUnstructuredMesh() : ugrid_edge(MAX_COLOURS),
                               ugridMapper_edge(MAX_COLOURS), 
                               ugridActor_edge(MAX_COLOURS) {
    m_bmesh.write("bhoomika");
    LoadMeshLevel(5, 0);
  }

/**
 * @brief 
 * 
 * @param level
 */
  void LoadMeshLevel(int level, int colour) {
    ugrid->Reset();
    for(auto &item : ugrid_edge)
      item->Reset();
    points->Reset();
    auto coord = m_bmesh.m_coords[level].data();
    auto nnodes = m_bmesh.m_coords[level].size() / 3;
    auto ntriangles = m_bmesh.m_triangles[level].size() / 3;
    auto triangle_nodes = m_bmesh.m_triangles[level].data();
    points->Allocate(nnodes);
    for (int i = 0; i < nnodes; i++)
      points->InsertPoint(i, coord + (i * 3));
    ugrid->Allocate(ntriangles);
    for (int i = 0; i < ntriangles; i++) {
      const vtkIdType conn[3] {
        triangle_nodes[i * 3],
        triangle_nodes[i * 3 + 1],
        triangle_nodes[i * 3 + 2]};
      ugrid->InsertNextCell(VTK_TRIANGLE, 3, conn);
    }
    ugrid->SetPoints(points);
    ugridMapper->SetInputData(ugrid);
    ugridActor->SetMapper(ugridMapper);
    ugridActor->GetProperty()->SetColor(
        colors->GetColor3d("Peacock").GetData());
    // ugridActor->GetProperty()->EdgeVisibilityOn();
    // ugridActor->GetProperty()->SetLineWidth(0.1);
    // ugridActor->GetProperty()->SetEdgeColor(
    //     colors->GetColor3d("ivory_black").GetData());
    // ugridActor->GetProperty()->SetRepresentationToWireframe();

    // Add edges of specified colour to the ugrid object
    if(1){ // TODO: remove to display proper coloured edges
      for (int ic = 0; ic < 2; ++ic) {
        auto nedges = m_bmesh.m_edges_colour_size[level][ic];
        auto edge_nodes = m_bmesh.m_edges[level].data() +
                          m_bmesh.m_edges_colour_ofs[level][ic];
        if (nedges > 0) {
          ugrid_edge[ic]->Allocate(nedges);
          for (int i = 0; i < nedges; i++) {
            const vtkIdType conn[2]{edge_nodes[i * 2], edge_nodes[i * 2 + 1]};
            ugrid_edge[ic]->InsertNextCell(VTK_LINE, 2, conn);
          }
          ugrid_edge[ic]->SetPoints(points);
          ugridMapper_edge[ic]->SetInputData(ugrid_edge[ic]);
          ugridActor_edge[ic]->SetMapper(ugridMapper_edge[ic]);
          ugridActor_edge[ic]->GetProperty()->SetColor(
            colors->GetColor3d(EDGE_COLOUR_MAP[ic]).GetData());
          // ugridActor_edge[ic]->GetProperty()->EdgeVisibilityOn();
          float line_width = (ic == colour) ? 5.0 : 1.0;
          ugridActor_edge[ic]->GetProperty()->SetLineWidth(line_width);
        }
      }
    }
  }

  /**
   * @brief
   *
   * @param renderer
   */
  void AddToRenderer(vtkRenderer &renderer) {
    renderer.AddActor(ugridActor);
    for(auto &item : ugridActor_edge)
      renderer.AddActor(item);
    renderer.SetBackground(colors->GetColor3d("Beige").GetData());
    // renderer.ResetCamera();
  }

  /**
   * @brief
   *
   * @param renderer
   */
  void RemoveFromRenderer(vtkRenderer &renderer) {
    renderer.RemoveActor(ugridActor);
    for(auto &item : ugridActor_edge)
      renderer.RemoveActor(item);
    // renderer.ResetCamera();
  }

};
