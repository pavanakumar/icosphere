#pragma once

#include <vtkConeSource.h>
#include <vtkGlyph3D.h>
#include <vtkSphereSource.h>
#include <vtkColor.h>
#include <vtkColorSeries.h>
#include <vtkNamedColors.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCellType.h>
#include <vtkPoints.h>

/**
 * @brief
 *
 */
struct ExampleConeSpike {

  vtkNew<vtkSphereSource> sphere;
  vtkNew<vtkPolyDataMapper> sphereMapper;
  vtkNew<vtkActor> sphereActor;
  vtkNew<vtkConeSource> cone;
  vtkNew<vtkGlyph3D> glyph;
  vtkNew<vtkPolyDataMapper> spikeMapper;
  vtkNew<vtkActor> spikeActor;

  /**
   * @brief Construct a new Example Cone Spike object
   *
   */
  ExampleConeSpike() {
    sphere->SetThetaResolution(8);
    sphere->SetPhiResolution(8);
    sphereMapper->SetInputConnection(sphere->GetOutputPort());
    sphereActor->SetMapper(sphereMapper);
    cone->SetResolution(6);
    glyph->SetInputConnection(sphere->GetOutputPort());
    glyph->SetSourceConnection(cone->GetOutputPort());
    glyph->SetVectorModeToUseNormal();
    glyph->SetScaleModeToScaleByVector();
    glyph->SetScaleFactor(0.25);
    spikeMapper->SetInputConnection(glyph->GetOutputPort());
    spikeActor->SetMapper(spikeMapper);
  }

  /**
   * @brief
   *
   * @param renderer
   */
  void AddToRenderer(vtkRenderer &renderer) {
    renderer.AddActor(sphereActor);
    renderer.AddActor(spikeActor);
  }

  /**
   * @brief
   *
   * @param renderer
   */
  void RemoveFromRenderer(vtkRenderer &renderer) {
    renderer.RemoveActor(sphereActor);
    renderer.RemoveActor(spikeActor);
    // renderer.ResetCamera();
  }

};

/**
 * @brief
 *
 */
struct ExampleUnstructuredMesh {

  const double c_global_x[27][3] {
      {0, 0, 0}, {1, 0, 0}, {2, 0, 0}, {0, 1, 0}, {1, 1, 0}, {2, 1, 0},
      {0, 0, 1}, {1, 0, 1}, {2, 0, 1}, {0, 1, 1}, {1, 1, 1}, {2, 1, 1},
      {0, 1, 2}, {1, 1, 2}, {2, 1, 2}, {0, 1, 3}, {1, 1, 3}, {2, 1, 3},
      {0, 1, 4}, {1, 1, 4}, {2, 1, 4}, {0, 1, 5}, {1, 1, 5}, {2, 1, 5},
      {0, 1, 6}, {1, 1, 6}, {2, 1, 6}};

  const vtkIdType c_global_pts[12][8] {
      {0, 1, 4, 3, 6, 7, 10, 9},      {1, 2, 5, 4, 7, 8, 11, 10},
      {6, 10, 9, 12, 0, 0, 0, 0},     {8, 11, 10, 14, 0, 0, 0, 0},
      {16, 17, 14, 13, 12, 15, 0, 0}, {18, 15, 19, 16, 20, 17, 0, 0},
      {22, 23, 20, 19, 0, 0, 0, 0},   {21, 22, 18, 0, 0, 0, 0, 0},
      {22, 19, 18, 0, 0, 0, 0, 0},    {23, 26, 0, 0, 0, 0, 0, 0},
      {21, 24, 0, 0, 0, 0, 0, 0},     {25, 0, 0, 0, 0, 0, 0, 0}};

  vtkNew<vtkPoints> points;
  vtkNew<vtkUnstructuredGrid> ugrid;
  vtkNew<vtkDataSetMapper> ugridMapper;
  vtkNew<vtkActor> ugridActor;
  vtkNew<vtkNamedColors> colors;

  /**
   * @brief Construct a new Example Unstructured Mesh object
   *
   */
  ExampleUnstructuredMesh() {

  for (int i = 0; i < 27; i++) points->InsertPoint(i, c_global_x[i]);
  ugrid->Allocate(100);
  ugrid->InsertNextCell(VTK_HEXAHEDRON, 8, c_global_pts[0]);
  ugrid->InsertNextCell(VTK_HEXAHEDRON, 8, c_global_pts[1]);
  ugrid->InsertNextCell(VTK_TETRA, 4, c_global_pts[2]);
  ugrid->InsertNextCell(VTK_TETRA, 4, c_global_pts[3]);
  ugrid->InsertNextCell(VTK_POLYGON, 6, c_global_pts[4]);
  ugrid->InsertNextCell(VTK_TRIANGLE_STRIP, 6, c_global_pts[5]);
  ugrid->InsertNextCell(VTK_QUAD, 4, c_global_pts[6]);
  ugrid->InsertNextCell(VTK_TRIANGLE, 3, c_global_pts[7]);
  ugrid->InsertNextCell(VTK_TRIANGLE, 3, c_global_pts[8]);
  ugrid->InsertNextCell(VTK_LINE, 2, c_global_pts[9]);
  ugrid->InsertNextCell(VTK_LINE, 2, c_global_pts[10]);
  ugrid->InsertNextCell(VTK_VERTEX, 1, c_global_pts[11]);

  ugrid->SetPoints(points);

  ugridMapper->SetInputData(ugrid);

  ugridActor->SetMapper(ugridMapper);
  ugridActor->GetProperty()->SetColor(colors->GetColor3d("Peacock").GetData());
  ugridActor->GetProperty()->EdgeVisibilityOn();

  }

  /**
   * @brief
   *
   * @param renderer
   */
  void AddToRenderer(vtkRenderer &renderer) {
    renderer.AddActor(ugridActor);
    renderer.SetBackground(colors->GetColor3d("Beige").GetData());
    // renderer.ResetCamera();
// #ifndef __EMSCRIPTEN__
//     renderer.GetActiveCamera()->Elevation(60.0);
//     renderer.GetActiveCamera()->Azimuth(30.0);
//     renderer.GetActiveCamera()->Dolly(1.2);
// #endif
  }

  /**
   * @brief
   *
   * @param renderer
   */
  void RemoveFromRenderer(vtkRenderer &renderer) {
    renderer.RemoveActor(ugridActor);
    // renderer.ResetCamera();
  }

};
