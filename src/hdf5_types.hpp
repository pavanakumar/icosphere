#pragma once

#include "hdf5.h"
#include <cstdint>
#include <vector>
#include <string>
#include <fmt/core.h>
#include <cassert>

namespace hdf5 {

/**
 * @brief Generic function to obtain HDF5 data type using template
 *
 * @tparam T
 * @return
 */
template <typename T> hid_t &type();

template <> hid_t &type<char>() { return H5T_NATIVE_CHAR; }

template <> hid_t &type<int8_t>() { return H5T_STD_I8LE; }
template <> hid_t &type<uint8_t>() { return H5T_STD_I8LE; }

template <> hid_t &type<int16_t>() { return H5T_STD_I16LE; }
template <> hid_t &type<uint16_t>() { return H5T_STD_I16LE; }

template <> hid_t &type<int32_t>() { return H5T_STD_I32LE; }
template <> hid_t &type<uint32_t>() { return H5T_STD_I32LE; }

template <> hid_t &type<int64_t>() { return H5T_STD_I64LE; }
template <> hid_t &type<uint64_t>() { return H5T_STD_I64LE; }

template <> hid_t &type<float>() { return H5T_IEEE_F32LE; }
template <> hid_t &type<double>() { return H5T_IEEE_F64LE; }

/**
 * @brief
 *
 * @tparam T
 * @param file
 * @param rank
 * @param dims
 * @param vlink
 * @param buf
 */
template <typename T>
static void write_dset(hid_t &file, const std::vector<hsize_t> dims,
                       const std::vector<const char *> vlink, const T *buf) {
  // Find if the groups exist
  std::string link("/");
  for (auto grp_name = vlink.begin(); grp_name != vlink.end() - 1; ++grp_name) {
    link += fmt::format("{}", *grp_name);
    if (H5Lexists(file, link.c_str(), H5P_DEFAULT) == 0) {
      auto grp =
          H5Gcreate(file, link.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
      H5Gclose(grp);
    }
    link += "/";
  }
  link += vlink.back();

  hid_t dset, dspace;
  herr_t status;
  dspace = H5Screate_simple(dims.size(), dims.data(), NULL);
  dset = H5Dcreate(file, link.c_str(), type<T>(), dspace, H5P_DEFAULT,
                   H5P_DEFAULT, H5P_DEFAULT);
  status = H5Dwrite(dset, type<T>(), H5S_ALL, H5S_ALL, H5P_DEFAULT, buf);
  assert(status >= 0);
  H5Sclose(dspace);
  H5Dclose(dset);
}

} // namespace hdf5
