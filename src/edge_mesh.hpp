#pragma once

#include <iostream>
#include <vector>

#include "hdf5_types.hpp"

static const int MAX_LEVELS = 7;
static const int MAX_COLOURS = 6;

/**
 * @brief
 *
 * @tparam T_GID
 * @tparam T_LID
 * @tparam T_REAL
 */
template <typename T_GID, typename T_LID, typename T_REAL> struct edge_mesh {

  typedef std::map<std::pair<T_LID, T_LID>, T_GID> index_map_t;

  std::vector<index_map_t> m_idx_map_topcap;
  std::vector<index_map_t> m_idx_map_belly;
  std::vector<index_map_t> m_idx_map_lowcap;
  std::vector<std::vector<T_GID>> m_edges;
  std::vector<std::vector<T_GID>> m_edges_colour_size;
  std::vector<std::vector<T_GID>> m_edges_colour_ofs;
  std::vector<std::vector<T_GID>> m_triangles;
  std::vector<std::vector<T_REAL>> m_coords;

  const T_REAL C_THETA_1 = 0.5 * M_PI - std::atan2(1.0, 2.0);
  const T_REAL C_THETA_2 = 0.5 * M_PI + std::atan2(1.0, 2.0);
  const T_REAL C_THETA_BELLY = 2.0 * std::atan2(1.0, 2.0);

  /**
   * @brief Get the imax object
   *
   * @param level
   * @return T_GID
   */
  T_GID get_imax(const T_GID level) { return (1 << level) + 1; }

  /**
   * @brief Construct a new edge mesh object
   *
   * @param level
   */
  edge_mesh()
      : m_idx_map_topcap(MAX_LEVELS), m_idx_map_belly(MAX_LEVELS),
        m_idx_map_lowcap(MAX_LEVELS), m_edges(MAX_LEVELS),
        m_edges_colour_size(MAX_LEVELS), m_edges_colour_ofs(MAX_LEVELS),
        m_triangles(MAX_LEVELS), m_coords(MAX_LEVELS) {
    std::vector<T_GID> inode_sizes;
    for (int i = 0; i < MAX_LEVELS; ++i) {
      // make basic node to i,j connectivity and first set of coord
      T_GID count = make_topcap(i, 0, m_idx_map_topcap[i], m_coords[i]);
      count = make_belly(i, count, m_idx_map_belly[i], m_coords[i]);
      count = make_lowcap(i, count, m_idx_map_lowcap[i], m_coords[i]);
      // make triangles
      make_topcap_triangles(i, m_idx_map_topcap[i], m_triangles[i]);
      make_belly_triangles(i, m_idx_map_belly[i], m_triangles[i]);
      make_lowcap_triangles(i, m_idx_map_lowcap[i], m_triangles[i]);
      // make edges
      m_edges_colour_size[i].resize(MAX_COLOURS);
      m_edges_colour_ofs[i].resize(MAX_COLOURS + 1);
      make_edges(i, m_idx_map_topcap[i], m_idx_map_belly[i],
                 m_idx_map_lowcap[i], m_edges[i], m_edges_colour_size[i],
                 m_edges_colour_ofs[i]);
    }
    // After all coordinate and connectivity construction
    // redo the coordinates of the fine level using coarse
    for (int i = 0; i < MAX_LEVELS - 1; ++i) {
      std::cout << "Level = " << i << "\n\n\n";
      fix_topcap(i, m_idx_map_topcap[i], m_coords[i], m_idx_map_topcap[i + 1],
                 m_coords[i + 1]);
      fix_belly(i, m_idx_map_belly[i], m_coords[i], m_idx_map_belly[i + 1],
                m_coords[i + 1]);
      fix_lowcap(i, m_idx_map_lowcap[i], m_coords[i], m_idx_map_lowcap[i + 1],
                 m_coords[i + 1]);
    }
    write_tec();
  }

  /**
   * @brief
   *
   * @param level
   * @param idx_map_topcap
   * @param idx_map_belly
   * @param idx_map_lowcap
   * @param edge
   * @param edge_colour_size
   */
  void make_edges(const T_GID level, index_map_t &idx_map_topcap,
                  index_map_t &idx_map_belly, index_map_t &idx_map_lowcap,
                  std::vector<T_GID> &edge,
                  std::vector<T_GID> &edge_colour_size,
                  std::vector<T_GID> &edge_colour_ofs) {

    const T_GID imax = get_imax(level);
    // first two colours
    for (int ic = 0; ic < 2; ++ic) {
      for (T_LID i = 1; i < imax; ++i) {
        for (T_LID j = ic; j < i * 5; j += 2) {
          edge.push_back(idx_map_topcap[{i, j}]);
          edge.push_back(idx_map_topcap[{i, j + 1}]);
          edge_colour_size[ic]++;
        }
      }
      for (T_LID i = 0; i < imax; ++i) {
        for (T_LID j = ic; j < 5 * (imax - 1); j += 2) {
          edge.push_back(idx_map_belly[{i, j}]);
          edge.push_back(idx_map_belly[{i, j + 1}]);
          edge_colour_size[ic]++;
        }
      }
      for (T_LID i = imax - 1; i >= 1; i--) {
        for (T_LID j = ic; j < i * 5; j += 2) {
          edge.push_back(idx_map_lowcap[{i, j}]);
          edge.push_back(idx_map_lowcap[{i, j + 1}]);
          edge_colour_size[ic]++;
        }
      }
    }
    // next two colours
    for (T_LID i = 0; i < imax - 1; ++i) {
      for (T_LID k = 0; k < 5; ++k) {
        for (T_LID jj = 0; jj <= i; ++jj) {
          edge.push_back(idx_map_topcap[{i, k * i + jj}]);
          edge.push_back(idx_map_topcap[{i + 1, k * (i + 1) + jj}]);
          edge_colour_size[2]++;
        }
      }
    }

    // Fix offsets using sizes
    for (int ic = 0; ic < MAX_COLOURS; ++ic)
      edge_colour_ofs[ic + 1] = edge_colour_ofs[ic] + edge_colour_size[ic] * 2;
  }

  /**
   * @brief
   *
   * @param level
   * @param idx_map_topcap
   * @param coord
   * @return T_GID
   */
  void fix_topcap(const T_GID level, index_map_t &idx_map_topcap,
                  const std::vector<T_REAL> &coord,
                  index_map_t &idx_map_topcap1, std::vector<T_REAL> &coord1) {

    const T_GID imax = get_imax(level);

    for (T_LID i = 0; i < imax - 1; ++i) {
      for (T_LID k = 0; k < 5; ++k) {
        const auto ofs1 = k * i;
        const auto ofs2 = k * (i + 1);
        for (T_LID jj = 0; jj <= i; ++jj) {
          const auto j = jj + ofs1;
          const auto jp1 = jj + ofs2;
          // 1. (i, j), (i + 1, jp1), (i + 1, jp1 + 1)
          // 2. (i, j), (i + 1, jp1 + 1), (i, j + 1)
          const T_LID ij[][3][2] = {{{i, j}, {i + 1, jp1}, {i + 1, jp1 + 1}},
                                    {{i, j}, {i + 1, jp1 + 1}, {i, j + 1}}};
          // skip the last triangle when jj == i
          auto it_end = (jj == i) ? 1 : 2;
          for (int it = 0; it < it_end; ++it) {
            for (int l = 0; l < 3; ++l) {
              T_LID cid[3], fid[3];
              cid[l] = idx_map_topcap[{ij[it][l][0], ij[it][l][1]}];
              fid[l] = idx_map_topcap1[{2 * ij[it][l][0], 2 * ij[it][l][1]}];
              for (int ll = 0; ll < 3; ++ll)
                coord1[3 * fid[l] + ll] = coord[3 * cid[l] + ll];
            }
            // Sum up the coordinates of edges and divide by norm
            // and update fine level coorindates by adding the i,j index
            const T_LID ijk[] = {0, 1, 2, 0};
            for (int ll = 0; ll < 3; ++ll) {
              auto l = ijk[ll];
              auto lp1 = ijk[ll + 1];
              auto fid = idx_map_topcap1[{ij[it][l][0] + ij[it][lp1][0],
                                          ij[it][l][1] + ij[it][lp1][1]}];
              auto cid = idx_map_topcap[{ij[it][l][0], ij[it][l][1]}];
              auto cid1 = idx_map_topcap[{ij[it][lp1][0], ij[it][lp1][1]}];
              T_REAL norm = 0.0;
              for (int il = 0; il < 3; ++il) {
                coord1[3 * fid + il] =
                    coord[3 * cid + il] + coord[3 * cid1 + il];
                norm += coord1[3 * fid + il] * coord1[3 * fid + il];
              }
              for (int il = 0; il < 3; ++il)
                coord1[3 * fid + il] /= std::sqrt(norm);
            }
          }
        }
      }
    }
  }

  /**
   * @brief
   *
   * @param level
   * @param offset
   * @param idx_map_topcap
   * @return T_GID
   */
  void fix_belly(const T_GID level, index_map_t &idx_map_belly,
                 const std::vector<T_REAL> &coord, index_map_t &idx_map_belly1,
                 std::vector<T_REAL> &coord1) {
    const T_GID imax = get_imax(level);
    for (T_LID i = 0; i < imax - 1; ++i) {
      for (T_LID j = 0; j < 5 * (imax - 1); ++j) {
        const T_LID ij[][3][2] = {{{i, j}, {i + 1, j}, {i, j + 1}},
                                  {{i, j + 1}, {i + 1, j}, {i + 1, j + 1}}};
        for (int it = 0; it < 2; ++it) {
          for (int l = 0; l < 3; ++l) {
            T_LID cid[3], fid[3];
            cid[l] = idx_map_belly[{ij[it][l][0], ij[it][l][1]}];
            fid[l] = idx_map_belly1[{2 * ij[it][l][0], 2 * ij[it][l][1]}];
            for (int ll = 0; ll < 3; ++ll)
              coord1[3 * fid[l] + ll] = coord[3 * cid[l] + ll];
          }
          // Sum up the coordinates of edges and divide by norm
          // and update fine level coorindates by adding the i,j index
          const T_LID ijk[] = {0, 1, 2, 0};
          for (int ll = 0; ll < 3; ++ll) {
            auto l = ijk[ll];
            auto lp1 = ijk[ll + 1];
            auto fid = idx_map_belly1[{ij[it][l][0] + ij[it][lp1][0],
                                       ij[it][l][1] + ij[it][lp1][1]}];
            auto cid = idx_map_belly[{ij[it][l][0], ij[it][l][1]}];
            auto cid1 = idx_map_belly[{ij[it][lp1][0], ij[it][lp1][1]}];
            T_REAL norm = 0.0;
            for (int il = 0; il < 3; ++il) {
              coord1[3 * fid + il] = coord[3 * cid + il] + coord[3 * cid1 + il];
              norm += coord1[3 * fid + il] * coord1[3 * fid + il];
            }
            for (int il = 0; il < 3; ++il)
              coord1[3 * fid + il] /= std::sqrt(norm);
          }
        }
      }
    }
  }

  /**
   * @brief
   *
   * @param level
   * @param idx_map_lowcap
   * @param coord
   * @param idx_map_lowcap1
   * @param coord1
   */
  void fix_lowcap(const T_GID level, index_map_t &idx_map_lowcap,
                  const std::vector<T_REAL> &coord,
                  index_map_t &idx_map_lowcap1, std::vector<T_REAL> &coord1) {
    const T_GID imax = get_imax(level);

    for (T_LID i = 0; i < imax - 1; ++i) {
      for (T_LID k = 0; k < 5; ++k) {
        auto ofs1 = k * i;
        auto ofs2 = k * (i + 1);
        for (T_LID jj = 0; jj <= i; ++jj) {
          auto j = jj + ofs1;
          auto j1 = jj + ofs2;
          // 1. (i, j), (i + 1, j), (i + 1, j + 1)
          // 2. (i, j), (i + 1, j + 1), (i, j + 1)
          const T_LID ij[][3][2] = {{{i, j}, {i + 1, j1}, {i + 1, j1 + 1}},
                                    {{i, j}, {i + 1, j1 + 1}, {i, j + 1}}};
          // skip the last triangle when jj == i
          auto it_end = (jj == i) ? 1 : 2;
          for (int it = 0; it < it_end; ++it) {
            for (int l = 0; l < 3; ++l) {
              T_LID cid[3], fid[3];
              cid[l] = idx_map_lowcap[{ij[it][l][0], ij[it][l][1]}];
              fid[l] = idx_map_lowcap1[{2 * ij[it][l][0], 2 * ij[it][l][1]}];
              for (int ll = 0; ll < 3; ++ll)
                coord1[3 * fid[l] + ll] = coord[3 * cid[l] + ll];
            }
            // Sum up the coordinates of edges and divide by norm
            // and update fine level coorindates by adding the i,j index
            const T_LID ijk[] = {0, 1, 2, 0};
            for (int ll = 0; ll < 3; ++ll) {
              auto l = ijk[ll];
              auto lp1 = ijk[ll + 1];
              auto fid = idx_map_lowcap1[{ij[it][l][0] + ij[it][lp1][0],
                                          ij[it][l][1] + ij[it][lp1][1]}];
              auto cid = idx_map_lowcap[{ij[it][l][0], ij[it][l][1]}];
              auto cid1 = idx_map_lowcap[{ij[it][lp1][0], ij[it][lp1][1]}];
              T_REAL norm = 0.0;
              for (int il = 0; il < 3; ++il) {
                coord1[3 * fid + il] =
                    coord[3 * cid + il] + coord[3 * cid1 + il];
                norm += coord1[3 * fid + il] * coord1[3 * fid + il];
              }
              for (int il = 0; il < 3; ++il)
                coord1[3 * fid + il] /= std::sqrt(norm);
            }
          }
        }
      }
    }
  }

  /**
   * @brief
   *
   * @param level
   * @param idx_map_topcap
   * @return T_GID
   */
  T_GID make_topcap(const T_GID level, const T_GID offset,
                    index_map_t &idx_map_topcap, std::vector<T_REAL> &coord) {

    const T_GID imax = get_imax(level);
    T_GID count = offset;
    idx_map_topcap[{0, 0}] = count++;

    coord.push_back(std::sin(0.0) * std::cos(0.0));
    coord.push_back(std::sin(0.0) * std::sin(0.0));
    coord.push_back(std::cos(0.0));

    const T_REAL dtheta_i = C_THETA_1 / (imax - 1);

    for (T_LID i = 1; i < imax; ++i) {
      const T_REAL dphi_j = 2.0 * M_PI / (i * 5);
      // even nodes
      for (T_LID j = 0; j <= i * 5; j += 2) {
        // Add only if the index is not already taken
        if (idx_map_topcap.find({i, j}) == std::end(idx_map_topcap)) {
          // These are the periodic nodes and we must use
          // the lower periodic IDs to avoid redundancy
          if (j == i * 5) {
            idx_map_topcap[{i, j}] = idx_map_topcap[{i, 0}];
          } else {
            idx_map_topcap[{i, j}] = count++;
            const auto theta = i * dtheta_i;
            const auto psi = j * dphi_j;
            coord.push_back(std::sin(theta) * std::cos(psi));
            coord.push_back(std::sin(theta) * std::sin(psi));
            coord.push_back(std::cos(theta));
          }
        }
      }

      // odd nodes
      for (T_LID j = 1; j <= i * 5; j += 2) {
        // Add only if the index is not already taken
        if (idx_map_topcap.find({i, j}) == std::end(idx_map_topcap)) {
          // These are the periodic nodes and we must use
          // the lower periodic IDs to avoid redundancy
          if (j == i * 5) {
            idx_map_topcap[{i, j}] = idx_map_topcap[{i, 0}];
          } else {
            idx_map_topcap[{i, j}] = count++;
            const auto theta = i * dtheta_i;
            const auto psi = j * dphi_j;
            coord.push_back(std::sin(theta) * std::cos(psi));
            coord.push_back(std::sin(theta) * std::sin(psi));
            coord.push_back(std::cos(theta));
          }
        }
      }
    }
    return count - (imax - 1) * 5;
  }

  /**
   * @brief
   *
   * @param level
   * @param offset
   * @param idx_map_belly
   * @return T_GID
   */
  T_GID make_belly(const T_GID level, const T_GID offset,
                   index_map_t &idx_map_belly, std::vector<T_REAL> &coord) {
    //
    const T_GID imax = get_imax(level);
    T_GID count = offset;
    const T_REAL dtheta_i = C_THETA_BELLY / (imax - 1);
    const T_REAL theta_ofs = C_THETA_1;
    const T_REAL theta_dpsi = M_PI / (5 * (imax - 1));

    for (T_LID i = 0; i < imax; ++i) {
      // even nodes
      for (T_LID j = 0; j <= 5 * (imax - 1); j += 2) {
        const T_REAL dphi_j = 2.0 * M_PI / (5 * (imax - 1));
        if (j == 5 * (imax - 1)) {
          idx_map_belly[{i, j}] = idx_map_belly[{i, 0}];
        } else {
          idx_map_belly[{i, j}] = count++;
          if (i != 0) {
            const auto theta = theta_ofs + i * dtheta_i;
            const auto psi = j * dphi_j + theta_dpsi * i;
            coord.push_back(std::sin(theta) * std::cos(psi));
            coord.push_back(std::sin(theta) * std::sin(psi));
            coord.push_back(std::cos(theta));
          }
        }
      }

      // odd nodes
      for (T_LID j = 1; j <= (imax - 1) * 5; j += 2) {
        const T_REAL dphi_j = 2.0 * M_PI / (5 * (imax - 1));
        if (j == (imax - 1) * 5) {
          idx_map_belly[{i, j}] = idx_map_belly[{i, 0}];
        } else {
          idx_map_belly[{i, j}] = count++;
          if (i != 0) {
            const auto theta = theta_ofs + i * dtheta_i;
            const auto psi = j * dphi_j + theta_dpsi * i;
            coord.push_back(std::sin(theta) * std::cos(psi));
            coord.push_back(std::sin(theta) * std::sin(psi));
            coord.push_back(std::cos(theta));
          }
        }
      }
    }
    return count - (imax - 1) * 5;
  }

  /**
   * @brief
   *
   * @param level
   * @param offset
   * @param idx_map_belly
   * @return T_GID
   */
  T_GID make_lowcap(const T_GID level, const T_GID offset,
                    index_map_t &idx_map_lowcap, std::vector<T_REAL> &coord) {
    //
    const T_GID imax = get_imax(level);
    T_GID count = offset;
    const T_REAL dtheta_i = C_THETA_1 / (imax - 1);
    const T_REAL theta_ofs = M_PI;
    const T_REAL theta_psi_ofs = M_PI / 5;

    for (T_LID i = imax - 1; i >= 1; i--) {
      const T_REAL dphi_j = 2.0 * M_PI / (i * 5);
      // even nodes
      for (T_LID j = 0; j <= i * 5; j += 2) {
        // Add only if the index is not already taken
        if (idx_map_lowcap.find({i, j}) == std::end(idx_map_lowcap)) {
          // These are the periodic nodes and we must use
          // the lower periodic IDs to avoid redundancy
          if (j == i * 5) {
            idx_map_lowcap[{i, j}] = idx_map_lowcap[{i, 0}];
          } else {
            idx_map_lowcap[{i, j}] = count++;
            if (i != imax - 1) {
              const auto theta = theta_ofs - i * dtheta_i;
              const auto psi = j * dphi_j + theta_psi_ofs;
              coord.push_back(std::sin(theta) * std::cos(psi));
              coord.push_back(std::sin(theta) * std::sin(psi));
              coord.push_back(std::cos(theta));
            }
          }
        }
      }

      // odd nodes
      for (T_LID j = 1; j <= i * 5; j += 2) {
        // Add only if the index is not already taken
        if (idx_map_lowcap.find({i, j}) == std::end(idx_map_lowcap)) {
          // These are the periodic nodes and we must use
          // the lower periodic IDs to avoid redundancy
          if (j == i * 5) {
            idx_map_lowcap[{i, j}] = idx_map_lowcap[{i, 0}];
          } else {
            idx_map_lowcap[{i, j}] = count++;
            if (i != imax - 1) {
              const auto theta = theta_ofs - i * dtheta_i;
              const auto psi = j * dphi_j + theta_psi_ofs;
              coord.push_back(std::sin(theta) * std::cos(psi));
              coord.push_back(std::sin(theta) * std::sin(psi));
              coord.push_back(std::cos(theta));
            }
          }
        }
      }
    }

    idx_map_lowcap[{0, 0}] = count;
    coord.push_back(std::sin(M_PI) * std::cos(0.0));
    coord.push_back(std::sin(M_PI) * std::sin(0.0));
    coord.push_back(std::cos(M_PI));
    return count;
  }

  /**
   * @brief
   *
   * @param level
   * @param offset
   * @param idx_map_topcap
   * @return T_GID
   */
  T_GID make_topcap_triangles(const T_GID level, index_map_t &idx_map_topcap,
                              std::vector<T_GID> &triangle) {
    const T_GID imax = get_imax(level);

    for (T_LID i = 0; i < imax - 1; ++i) {
      for (T_LID k = 0; k < 5; ++k) {
        auto ofs1 = k * i;
        auto ofs2 = k * (i + 1);
        for (T_LID jj = 0; jj <= i; ++jj) {
          auto j = jj + ofs1;
          auto j1 = jj + ofs2;
          // 1. (i, j), (i + 1, j), (i + 1, j + 1)
          // 2. (i, j), (i + 1, j + 1), (i, j + 1)
          // Triangle 1
          triangle.push_back(idx_map_topcap[{i, j}]);
          triangle.push_back(idx_map_topcap[{i + 1, j1}]);
          triangle.push_back(idx_map_topcap[{i + 1, j1 + 1}]);
          if(!(k==5 && jj == i)) {
            // Triangle 2
            triangle.push_back(idx_map_topcap[{i, j}]);
            triangle.push_back(idx_map_topcap[{i + 1, j1 + 1}]);
            triangle.push_back(idx_map_topcap[{i, j + 1}]);
          }
        }
      }
    }
    return triangle.size();
  }

  /**
   * @brief
   *
   * @param level
   * @param offset
   * @param idx_map_topcap
   * @return T_GID
   */
  T_GID make_belly_triangles(const T_GID level, index_map_t &idx_map_belly,
                             std::vector<T_GID> &triangle) {
    const T_GID imax = get_imax(level);
    for (T_LID i = 0; i < imax - 1; ++i) {
      for (T_LID j = 0; j < 5 * (imax - 1); ++j) {
        // Triangle 1
        triangle.push_back(idx_map_belly[{i, j}]);
        triangle.push_back(idx_map_belly[{i + 1, j}]);
        triangle.push_back(idx_map_belly[{i, j + 1}]);
        // Triangle 2
        triangle.push_back(idx_map_belly[{i, j + 1}]);
        triangle.push_back(idx_map_belly[{i + 1, j}]);
        triangle.push_back(idx_map_belly[{i + 1, j + 1}]);
      }
    }
    return triangle.size();
  }

  /**
   * @brief
   *
   * @param level
   * @param offset
   * @param idx_map_topcap
   * @return T_GID
   */
  T_GID make_lowcap_triangles(const T_GID level, index_map_t &idx_map_lowcap,
                              std::vector<T_GID> &triangle) {
    const T_GID imax = get_imax(level);

    for (T_LID i = 0; i < imax - 1; ++i) {
      for (T_LID k = 0; k < 5; ++k) {
        auto ofs1 = k * i;
        auto ofs2 = k * (i + 1);
        for (T_LID jj = 0; jj <= i; ++jj) {
          auto j = jj + ofs1;
          auto j1 = jj + ofs2;
          // 1. (i, j), (i + 1, j), (i + 1, j + 1)
          // 2. (i, j), (i + 1, j + 1), (i, j + 1)
          // Triangle 1
          triangle.push_back(idx_map_lowcap[{i, j}]);
          triangle.push_back(idx_map_lowcap[{i + 1, j1}]);
          triangle.push_back(idx_map_lowcap[{i + 1, j1 + 1}]);
          if (!(k == 5 && jj == i)) {
            // Triangle 2
            triangle.push_back(idx_map_lowcap[{i, j}]);
            triangle.push_back(idx_map_lowcap[{i + 1, j1 + 1}]);
            triangle.push_back(idx_map_lowcap[{i, j + 1}]);
          }
        }
      }
    }
    return triangle.size();
  }

  /**
   * @brief
   *
   */
  void write(const char *file_name) {
    auto h5_file_name = fmt::format("{}.h5", file_name);
    auto file = H5Fcreate(h5_file_name.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT,
                          H5P_DEFAULT);
    for (int i = 0; i < MAX_LEVELS; ++i)
      write_hdf5_grid(i, file, "level_0");
    H5Fclose(file);
  }

  /**
   * @brief
   *
   * @param level
   * @param hid
   * @param str
   */
  void write_hdf5_grid(int level, hid_t &hid, const char *str) {}

  /**
   * @brief
   *
   */
  void write_tec() {
    for (int i = 0; i < MAX_LEVELS; ++i) {
      auto fname =
          std::string("triangle_") + std::to_string(i) + std::string(".dat");
      std::ofstream fout(fname);
      fout << "VARIABLES=\"X\", \"Y\", \"Z\"\n";
      fout << "ZONE DATAPACKING=BLOCK, NODES=" << m_coords[i].size() / 3
           << ", ELEMENTS=" << m_triangles[i].size() / 3
           << ", ZONETYPE=FETRIANGLE\n";
      for (T_GID idx = 0; idx < m_coords[i].size() / 3; ++idx)
        fout << m_coords[i][idx * 3] << "\n";
      for (T_GID idx = 0; idx < m_coords[i].size() / 3; ++idx)
        fout << m_coords[i][idx * 3 + 1] << "\n";
      for (T_GID idx = 0; idx < m_coords[i].size() / 3; ++idx)
        fout << m_coords[i][idx * 3 + 2] << "\n";
      for (const auto &it : m_triangles[i])
        fout << it + 1 << "\n";
    }
  }

};
