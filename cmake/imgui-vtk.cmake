set(imgui_sources
        external/imgui/imgui.cpp
        external/imgui/imgui_widgets.cpp
        external/imgui/imgui_tables.cpp
        external/imgui/imgui_draw.cpp
        external/imgui/imgui_demo.cpp
        external/imgui/backends/imgui_impl_opengl3.cpp
        external/imgui/backends/imgui_impl_sdl2.cpp
        external/ImGuiFileDialog/ImGuiFileDialog.cpp
        )
set(imgui_headers_common
        external/imgui/imconfig.h
        external/imgui/imgui.h
        external/imgui/imgui_internal.h
        external/imgui/imstb_rectpack.h
        external/imgui/imstb_textedit.h
        external/imgui/imstb_truetype.h
        external/ImGuiFileDialog/ImGuiFileDialog.h
        external/ImGuiFileDialog/ImGuiFileDialogConfig.h
        )
set(imgui_headers_opengl
        external/imgui/backends/imgui_impl_opengl3.h
        )
set(imgui_headers_glfw_sdl
        external/imgui/backends/imgui_impl_sdl.h
        )
set(imgui_headers_all
    ${imgui_headers_common}
    ${imgui_headers_opengl}
    ${imgui_headers_sdl}
    )

if(NOT CMAKE_SYSTEM_NAME MATCHES Emscripten)
  if (APPLE)
      find_library(COCOA_LIBRARY Cocoa)
      find_library(OpenGL_LIBRARY OpenGL)
      find_library(IOKIT_LIBRARY IOKit)
      find_library(COREVIDEO_LIBRARY CoreVideo)
      set(EXTRA_LIBS ${COCOA_LIBRARY} ${OpenGL_LIBRARY} ${IOKIT_LIBRARY} ${COREVIDEO_LIBRARY})
  endif (APPLE)
  
  if(NOT _vtk_supports_sdl2)
    find_package(SDL2 CONFIG REQUIRED)
    find_package(GLEW REQUIRED)
  endif()
  
  # opengl
  find_package(OpenGL REQUIRED)
endif()

add_library(imgui-vtk ${imgui_sources})
add_library(vtkImGuiAdapter::imgui-vtk ALIAS imgui-vtk)
target_link_libraries(imgui-vtk PUBLIC OpenGL::GL)

if(CMAKE_SYSTEM_NAME MATCHES Emscripten)
  target_compile_options(imgui-vtk PUBLIC -sUSE_SDL=2)
  target_link_options(imgui-vtk PUBLIC -sUSE_SDL=2)
else()

  if(NOT _vtk_supports_sdl2)
    target_link_libraries(imgui-vtk PUBLIC SDL2::SDL2)
    target_link_libraries(imgui-vtk PUBLIC GLEW::GLEW)
  endif()
endif()

target_include_directories(imgui-vtk PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/external/imgui>
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/external/imgui/backends>
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/external/imgui/examples/example_sdl2_opengl3>
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/external/ImGuiFileDialog>
  $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
  )

install(TARGETS imgui-vtk
        EXPORT vtkImGuiAdapterTargets
        RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT runtime
        LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT runtime
        ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT development
        )

install(FILES ${imgui_headers_all}
  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/external/vtkImGuiAdapter)

# export to the build tree
export( TARGETS imgui-vtk
  NAMESPACE vtkImGuiAdapter::
  APPEND FILE ${vtkImGuiAdapter_export_file})
