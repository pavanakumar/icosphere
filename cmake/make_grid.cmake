include(vtkImGuiAdapter)

add_executable(make_grid src/make_grid_app.cpp)
target_link_libraries(make_grid PUBLIC vtkImGuiAdapter fmt::fmt)

if(CMAKE_SYSTEM_NAME MATCHES Emscripten)

  set(emscripten_compile_flags)
  list(APPEND emscripten_compile_flags
    -Oz
    -g1
    -sUSE_SDL=2
    -sFULL_ES3
    -sUSE_ZLIB=1
    -sELIMINATE_DUPLICATE_FUNCTIONS=1)

  set(emscripten_link_flags)
  list(APPEND emscripten_link_flags
   ${emscripten_compile_flags}
   --bind
   --shell-file ${CMAKE_SOURCE_DIR}/html/shell_min.html
   -sFORCE_FILESYSTEM=1
   -sEXPORTED_RUNTIME_METHODS=[FS]
   -sEXPORTED_FUNCTIONS=[_main,_malloc,_free]
   -sALLOW_MEMORY_GROWTH
   -sINITIAL_MEMORY=64MB
   -sSTACK_SIZE=10MB)

  set(CMAKE_EXECUTABLE_SUFFIX ".html")
  target_link_libraries(make_grid PUBLIC hdf5-wasm-cpp)
  target_include_directories(make_grid PUBLIC external/emscripten-browser-file)
  target_compile_options(make_grid PUBLIC ${emscripten_compile_flags})
  target_link_options(make_grid PUBLIC ${emscripten_link_flags})

else()

  target_link_libraries(make_grid PRIVATE hdf5-static)

endif()

target_include_directories(make_grid PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>/src
  $<BUILD_INTERFACE:${CMAKE_BINARY_DIR}>)

# target_compile_definitions(make_grid PUBLIC SHOW_EXAMPLE)

vtk_module_autoinit(
  TARGETS make_grid
  MODULES ${VTK_LIBRARIES})
