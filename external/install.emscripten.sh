#!/bin/sh
# Change this to point to the right MPI C,C++ compiler

INSTALL_PATH=$PWD/DIST
ROOT_DIR=$PWD
EXTRA=$PWD/extra

NUM_PROCS=4

# Install VTK wasm
cd $ROOT_DIR
tar -jxvf vtk-wasm.tar.bz2

