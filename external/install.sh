#!/bin/sh
# Change this to point to the right MPI C,C++ compiler
export CC=gcc
export CXX=g++

NUM_PROCS=4

##############################################
############  DO NOT MODIFY #################
##############################################
INSTALL_PATH=$PWD/DIST
ROOT_DIR=$PWD
EXTRA=$PWD/extra

# Install GLEW
cd $ROOT_DIR
tar -zxvf glew-2.1.0.tgz
cd glew-2.1.0
cd build/cmake
CC=$CC CXX=$CXX FC=$FC cmake -DCMAKE_INSTALL_PREFIX=$INSTALL_PATH .
make -j$NUM_PROCS install && cd $ROOT_DIR && rm -rf glew-2.1.0

# Install SDL2
cd $ROOT_DIR
tar -zxvf SDL2-2.26.3.tar.gz
cd SDL2-2.26.3
mkdir build
cd build
CC=$CC CXX=$CXX FC=$FC cmake -DCMAKE_INSTALL_PREFIX=$INSTALL_PATH ..
make -j$NUM_PROCS install && cd $ROOT_DIR && rm -rf SDL2-2.26.3

# Install VTK
cd $ROOT_DIR
tar -zxvf VTK-9.2.6.tar.gz
cd VTK-9.2.6
mkdir build
cd build
CC=$CC CXX=$CXX FC=$FC SDL2_DIR=$INSTALL_PATH cmake -DVTK_USE_SDL2=BOOL:ON -DCMAKE_INSTALL_PREFIX=$INSTALL_PATH ..
make -j$NUM_PROCS install && cd $ROOT_DIR && rm -rf VTK-9.2.6
