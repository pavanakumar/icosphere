CMAKE_MINIMUM_REQUIRED (VERSION 3.13)

PROJECT (icosphere VERSION 1.0.0)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)
LIST (APPEND CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake")
SET (CMAKE_CXX_STANDARD 17)
SET (CMAKE_CXX_STANDARD_REQUIRED ON)
enable_language(CXX C)

add_subdirectory(external/fmt)

if(CMAKE_SYSTEM_NAME MATCHES Emscripten)
  include(FetchContent)

  message("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
  message("^^^^^^^^^^ Enabling emscripten compile ^^^^^^^^^^^")
  message("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")

  # Fix for supressing warning about DOWNLOAD_EXTRACT_TIMESTAMP
  if (CMAKE_VERSION VERSION_GREATER_EQUAL "3.24.0")
    cmake_policy(SET CMP0135 NEW)
  endif()

  FetchContent_Declare(
    libhdf5-wasm
    URL https://github.com/usnistgov/libhdf5-wasm/releases/download/v0.2.1_3.1.8/libhdf5-1_12_1-wasm.tar.gz
    URL_HASH SHA256=93300cffaf3084e487c3214cdf8e0ba407cc62ac2c6e14261160d29ec0dc1a06
  )
  FetchContent_MakeAvailable(libhdf5-wasm)

else()

  # Use HDF5 from sources to avoid clashing with system installed one
  set(HDF5_EXTERNALLY_CONFIGURED 1)
  set(BUILD_TESTING OFF CACHE BOOL "Build HDF5 Unit Testing")
  set(BUILD_SHARED_LIBS OFF CACHE BOOL "Build Shared Libraries")
  set(HDF5_BUILD_CPP_LIB OFF CACHE BOOL "Build HDF5 C++ Library")
  set(HDF5_BUILD_HL_LIB OFF CACHE BOOL "Build HIGH Level HDF5 Library")
  set(HDF5_BUILD_TOOLS OFF CACHE BOOL "Build HDF5 Tools")
  set(HDF5_BUILD_EXAMPLES OFF CACHE BOOL "Build HDF5 Library Examples")
  set(HDF5_GENERATE_HEADERS OFF CACHE BOOL "Turn off header generation for HDF5" FORCE)
  set(HDF5_DISABLE_COMPILER_WARNINGS ON CACHE BOOL "Turn off HDF5 compiler warnings" FORCE)
  add_subdirectory(external/hdf5-1.10.8)

endif()

include(make_grid)
